package org.example.grade;

public class Employee {

    private String nom;
    private int age;
    private double salaire;

    public Employee(String nom, int age, double salaire) {
        this.nom = nom;
        this.age = age;
        this.salaire = salaire;
    }

    public void augmentation(double pourcentage) {
        this.salaire *= (1 + pourcentage/100);
    }

    public void afficher() {
        System.out.println("Nom : " + this.nom);
        System.out.println("Age : " + this.age);
        System.out.println("Salaire : " + this.salaire);
    }

    public double calculeSalaire() {
        return this.salaire;
    }

}
