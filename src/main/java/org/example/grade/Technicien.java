package org.example.grade;

public class Technicien extends Employee{
    private int grade;

    public Technicien(String nom, int age, double salaire, int grade) {
        super(nom, age, salaire);
        this.grade = grade;
    }

    public int prime() {
        if (this.grade == 1) {
            return 100;
        } else if (this.grade == 2) {
            return 200;
        } else if (this.grade == 3) {
            return 300;
        } else {
            return 0;
        }
    }

    public double calculeSalaire() {
        return super.calculeSalaire() + prime();
    }
}
