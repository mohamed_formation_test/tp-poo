package org.example;

import org.example.grade.Employee;
import org.example.grade.Technicien;
import org.example.house.Apartment;
import org.example.house.Person;
import org.example.salarie.Salarie;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Apartment apt = new Apartment();
       Person person = new Person("Thomas", apt);
       person.display();

  //      Salarie toto = new Salarie("Toto", 2000);
//        Salarie titi = new Salarie("Titi", 2300);
//        Salarie tata = new Salarie("Toto", 3000);
//        Salarie tete = new Salarie("Toto", 3500);
//
//        System.out.println("Le nombre de salarié(s) : " + Salarie.getCount());
//
//        List<Salarie> liste = new ArrayList<Salarie>();
//
//        liste.add(toto);
//        liste.add(titi);
//        liste.add(tata);
//        liste.add(tete);
//
//        int sum = 0;
//
//        for (Salarie s: liste ) {
//            s.afficherSalaire();
//            sum += s.getSalaire();
//        }
//
//        System.out.println("Le montant total des salaires est de " + sum);
//        Salarie.setCount(1);
//        System.out.println("Le nombre de salarié(s) : " + Salarie.getCount());

        Scanner input = new Scanner(System.in);

        // Saisie d'un employé
        System.out.println("Saisie d'un employé :");
        System.out.print("Nom : ");
        String nom = input.next();
        System.out.print("Age : ");
        int age = input.nextInt();
        System.out.print("Salaire : ");
        double salaire = input.nextDouble();

        Employee employe = new Employee(nom, age, salaire);
        employe.afficher();

        System.out.print("Augmentation de salaire en pourcentage : ");
        double pourcentage = input.nextDouble();
        employe.augmentation(pourcentage);
        employe.afficher();

        // Saisie d'un technicien
        System.out.println("Saisie d'un technicien :");
        System.out.print("Nom : ");
        nom = input.next();
        System.out.print("Age : ");
        age = input.nextInt();
        System.out.print("Salaire : ");
        salaire = input.nextDouble();
        System.out.print("Grade : ");
        int grade = input.nextInt();

        Technicien technicien = new Technicien(nom, age, salaire, grade);
        technicien.afficher();

        System.out.print("Augmentation de salaire en pourcentage : ");
        pourcentage = input.nextDouble();
        technicien.augmentation(pourcentage);
        technicien.afficher();




    }



}